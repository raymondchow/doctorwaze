//
//  DoctorWazeAPITest.swift
//  DoctorWaze
//
//  Created by Raymond Chow Chee Seng on 03/08/2016.
//  Copyright © 2016 Raymond Chow Chee Seng. All rights reserved.
//

import XCTest
@testable import DoctorWaze

class DoctorWazeAPITest: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func createTaskForClinic (clinic: Clinic, fileName: String, expectation: XCTestExpectation) -> NSURLSessionDataTask{
        
        // Define the url
        let url: NSURL = NSURL (string: "https://api.backendless.com/v1/data/Clinic")!
        // need to load an URL
        let sessionConfig: NSURLSessionConfiguration = NSURLSessionConfiguration.defaultSessionConfiguration()
        sessionConfig.HTTPAdditionalHeaders = ["application-id" : "AEECCF30-FAE7-BF12-FFC6-2E0AD139DB00",
                                               "secret-key" : "BEC01580-9EC4-D4B7-FF3A-3D2EF2198100",
                                               "Content-type" : "application/json",
                                               "application-type" : "REST"
        ]
        
        // Assign  configuration to Session
        let session: NSURLSession = NSURLSession(configuration: sessionConfig)
        
        // Define the URL Request Object
        // NSMutableRequest
        let urlRequest: NSMutableURLRequest = NSMutableURLRequest(URL: url)
        
        // Set the METHOD for URL Request
        urlRequest.HTTPMethod = "POST"
        
        // Test create a Clinic object
        let newClinic = clinic
        
        // Define a JSON object for the Clinic object created
        let jsonClinic: [String : AnyObject] = ["ClinicName" : newClinic.name,
                                                "ClinicEmail" : newClinic.email ??  NSNull(),
                                                "ImageFile" : fileName]
        
        // Create a JSON DATA
        // Serialize the JSON Object to JSON Data
        let jsonClinicData: NSData = try! NSJSONSerialization.dataWithJSONObject(jsonClinic, options: NSJSONWritingOptions(rawValue: 0))
        
        // Set the BODY of the URL Request
        // Usually expecting JSON Data
        urlRequest.HTTPBody = jsonClinicData
        
        // Define the session task
        // This time is using Session Task with URL Request
        let postTask: NSURLSessionDataTask = session.dataTaskWithRequest(urlRequest) { (data: NSData?, response: NSURLResponse?, error: NSError?) in
            //code
            // Check response code
            if let responseCode: NSHTTPURLResponse = response as? NSHTTPURLResponse {
                XCTAssert(responseCode.statusCode == 200, "Connected with the right code")
            }
            else{
                XCTAssert(false, "NOT RIGHT")
            }
            
            expectation.fulfill()
        }
        
        return postTask
        
    }
    /*
    func testLoadingImageFromServer(){
        
        // Set Expectation
        let loadingImageExpectation = self.expectationWithDescription("Loading Image")
        
        let fileURL: NSURL = NSURL(fileURLWithPath: )
        
        
        
    }
    */
    
    func testCreatingImageToServer(){
        // Set Expectation (this piece is for testing to hold asynchronous)
        let createImageExpectation = self.expectationWithDescription("Create Image")
        let createClinicExpectation = self.expectationWithDescription("Create Clinic")
        
        // need to load an URL
        let sessionConfig: NSURLSessionConfiguration = NSURLSessionConfiguration.defaultSessionConfiguration()
        
        sessionConfig.HTTPAdditionalHeaders = ["application-id" : "AEECCF30-FAE7-BF12-FFC6-2E0AD139DB00",
                                               "secret-key" : "BEC01580-9EC4-D4B7-FF3A-3D2EF2198100",
                                               "Content-Type" : "multipart/form-data",
                                               "application-type" : "REST"
        ]
        
        // Assign  configuration to Session
        let session: NSURLSession = NSURLSession(configuration: sessionConfig)
        
    
        // Define the url of the image in the backend server
        // Need to refer to the backend API on the URL pattern
        let url: NSURL = NSURL(string: "https://api.backendless.com/v1/files/media/image001.jpeg?overwrite=true")!
        
        // Create an URL Request object
        let urlRequest: NSMutableURLRequest = NSMutableURLRequest(URL: url)
        
        // Define the URL Request method
        urlRequest.HTTPMethod = "POST"
        
        urlRequest.addValue("multipart/form-data; boundary=\"_PARKLANDS_BOUNDARY_\"", forHTTPHeaderField: "Content-Type")
        
        let boundaryHeader: NSMutableString = NSMutableString()
        boundaryHeader.appendString("--_PARKLANDS_BOUNDARY_\n")
        boundaryHeader.appendString("Content-Disposition: form-data; name=\"file\"; filename=\"image001\"\n")
        boundaryHeader.appendString("Content-Type: image/jpg\n\n")
        
        let bodyData: NSMutableData = NSMutableData()
        bodyData.appendData(boundaryHeader.dataUsingEncoding(NSASCIIStringEncoding)!)
        
        
        //This setting is required if we define a common session
        //urlRequest.addValue = ("image/jpeg", forHTTPHeaderField: "Content-type")
        
        // Image file to use
        let image: UIImage = UIImage(named: "clinic001")!
        let imageData: NSData = UIImageJPEGRepresentation(image, 0.9)!
        bodyData.appendData(imageData)
        
        // Define an Session Task
        // For upload, use Session Upload Task
        let sessionUploadTask: NSURLSessionUploadTask = session.uploadTaskWithRequest(urlRequest, fromData: bodyData) { (data: NSData?, response: NSURLResponse?, error: NSError?) in
            //<#code#>
            // Check response code
            if let responseCode: NSHTTPURLResponse = response as? NSHTTPURLResponse {
                XCTAssert(responseCode.statusCode == 200, "Connected with the right code")
            }
            else{
                XCTAssert(false, "NOT RIGHT")
            }
            
            // Try to get the file url return
            let jsonString :String? = String(data: data!, encoding: NSUTF8StringEncoding)
            
            // Test create a Clinic object
            let newClinic: Clinic = Clinic(name: "Clinic ADDED 003", address: "", contactNumber: "")
            
            // Get the file name from the response json
            let jsonDict: [String : AnyObject] = try! NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions(rawValue: 0) ) as! [String : AnyObject]
            
            let fileName: String = jsonDict["fileURL"] as! String
            
            //Run Another task to create Clinic object
            let createTask: NSURLSessionDataTask = self.createTaskForClinic(newClinic, fileName: fileName, expectation: createClinicExpectation)
            createTask.resume()
            
            print (fileName)
            print("This is the file \(jsonString)")
            
            createImageExpectation.fulfill()
            
        }
        sessionUploadTask.resume()
        self.waitForExpectationsWithTimeout(10, handler: nil)
        
    }
    
    func testCreateClinicToBackEndless(){
        
        // Set Expectation (this piece is for testing to hold asynchronous)
        let expectation = self.expectationWithDescription("Create Clinic")
        
        // Define the url
        let url: NSURL = NSURL (string: "https://api.backendless.com/v1/data/Clinic")!
        
        // need to load an URL
        let sessionConfig: NSURLSessionConfiguration = NSURLSessionConfiguration.defaultSessionConfiguration()
        
        sessionConfig.HTTPAdditionalHeaders = ["application-id" : "AEECCF30-FAE7-BF12-FFC6-2E0AD139DB00",
                                               "secret-key" : "BEC01580-9EC4-D4B7-FF3A-3D2EF2198100",
                                               "Content-type" : "application/json",
                                               "application-type" : "REST"
        ]
        
        // Assign  configuration to Session
        let session: NSURLSession = NSURLSession(configuration: sessionConfig)
        
        // Define the URL Request Object
        // NSMutableRequest
        let urlRequest: NSMutableURLRequest = NSMutableURLRequest(URL: url)
        
        // Set the METHOD for URL Request
        urlRequest.HTTPMethod = "POST"
        
        // Test create a Clinic object
        let newClinic: Clinic = Clinic(name: "Clinic ADDED 003", address: "", contactNumber: "")
        
        // Define a JSON object for the Clinic object created
        let jsonClinic: [String : AnyObject] = ["clinicName" : newClinic.name,
                                                "email" : newClinic.email ??  NSNull()]
    
        
    
        // Create a JSON DATA
        // Serialize the JSON Object to JSON Data
        let jsonClinicData: NSData = try! NSJSONSerialization.dataWithJSONObject(jsonClinic, options: NSJSONWritingOptions(rawValue: 0))
        
        // Set the BODY of the URL Request
        // Usually expecting JSON Data
        urlRequest.HTTPBody = jsonClinicData
        
        // Define the session task
        // This time is using Session Task with URL Request
        let postTask: NSURLSessionDataTask = session.dataTaskWithRequest(urlRequest) { (data: NSData?, response: NSURLResponse?, error: NSError?) in
            //code
            // Check response code
            if let responseCode: NSHTTPURLResponse = response as? NSHTTPURLResponse {
                XCTAssert(responseCode.statusCode == 200, "Connected with the right code")
            }
            else{
                XCTAssert(false, "NOT RIGHT")
            }
            
            expectation.fulfill()
        }
        postTask.resume()
        
        self.waitForExpectationsWithTimeout(10, handler: nil)
        
        
    }
    
    func testLoadUsingLoader() {
        let expectation = self.expectationWithDescription("Load Something")

        ClinicLoader.clinicLoader.getClinicsFromServer { (clinics) in
            XCTAssert(clinics != nil, "Should return something")
            
            if let clinics = clinics {
                XCTAssert(clinics.count > 0, "Should have loaded more than 1 clinic")
            }

            expectation.fulfill()
        }
        
        self.waitForExpectationsWithTimeout(20, handler: nil)
    }
      
    func testLoadingPage() {
        
        let expectation = self.expectationWithDescription("Load Task")
        
        let session : NSURLSession = NSURLSession.sharedSession()
        let url: NSURL = NSURL(string: "http://apple.com")!
        
        let loadtask : NSURLSessionDataTask = session.dataTaskWithURL(url) { (data: NSData?, response: NSURLResponse?, error: NSError?) -> Void in
            // code
            let htmlString: String? = String(data: data!, encoding: NSUTF8StringEncoding)
            
            XCTAssert(htmlString != nil, "CORRECT")
            
            NSLog("Task Completed")
            expectation.fulfill()
            
        }
        
        loadtask.resume()
        
        self.waitForExpectationsWithTimeout(10, handler: nil)
        
    }
    

    
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measureBlock {
            // Put the code you want to measure the time of here.
        }
    }

}
