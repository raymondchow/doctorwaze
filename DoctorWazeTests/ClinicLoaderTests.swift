//
//  ClinicLoaderTests.swift
//  DoctorWaze
//
//  Created by Raymond Chow Chee Seng on 30/07/2016.
//  Copyright © 2016 Raymond Chow Chee Seng. All rights reserved.
//

import XCTest
@testable import DoctorWaze

class ClinicLoaderTests: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measureBlock {
            // Put the code you want to measure the time of here.
        }
    }
    
    func testToWriteToFile(){
        
        let fileManager: NSFileManager = NSFileManager.defaultManager()
        let documentsDirectory: NSURL = fileManager.URLsForDirectory(NSSearchPathDirectory.DocumentDirectory, inDomains: NSSearchPathDomainMask.UserDomainMask).first!
        
        let filePath: NSURL = documentsDirectory.URLByAppendingPathComponent("data.plist")
        
        var clinicListing: [Clinic] = []
        
        var newClinic001: Clinic = Clinic(name: "Clinic001", address: "", contactNumber: "")
        var newClinic002: Clinic = Clinic(name: "Clinic002", address: "", contactNumber: "")
        
        clinicListing += [newClinic001]
        clinicListing += [newClinic002]
        
        var clinicsData: [NSData] = []
        
        for clinic in clinicListing{
            let data: NSData = NSKeyedArchiver.archivedDataWithRootObject(clinic)
            clinicsData += [data]
        }
        
        (clinicsData as NSArray).writeToURL(filePath, atomically: true)
        
        var checkFileExists: Bool = fileManager.fileExistsAtPath(filePath.path!)
        
        XCTAssert(checkFileExists, "File is there")
        
        NSLog("File path is \(filePath.path!)")
        
    }
    
    func testReadingFile(){
        let fileManager: NSFileManager = NSFileManager.defaultManager()
        let documentsDirectory: NSURL = fileManager.URLsForDirectory(NSSearchPathDirectory.DocumentDirectory, inDomains: NSSearchPathDomainMask.UserDomainMask).first!
        let filePath: NSURL = documentsDirectory.URLByAppendingPathComponent("data.plist")
        
        let parkData: [NSData] = NSArray(contentsOfURL: filePath) as! [NSData]
        
        var clinicListing: [Clinic] = []
        
        
        for recordData in parkData {
            let clinic: Clinic = NSKeyedUnarchiver.unarchiveObjectWithData(recordData) as! Clinic
            clinicListing += [clinic]
        }
        
        XCTAssert(clinicListing.count > 1)
        
        
    }

}
