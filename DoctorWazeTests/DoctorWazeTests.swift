//
//  DoctorWazeTests.swift
//  DoctorWazeTests
//
//  Created by Raymond Chow Chee Seng on 16/07/2016.
//  Copyright © 2016 Raymond Chow Chee Seng. All rights reserved.
//

import XCTest
@testable import DoctorWaze

class DoctorWazeTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measureBlock {
            // Put the code you want to measure the time of here.
        }
    }
    
    func testSaveClinics(){
        let userDefaults: NSUserDefaults = NSUserDefaults.standardUserDefaults()
        
        let clinic: Clinic = Clinic(name: "ClinicABC", address: "Cyberjaya", contactNumber: "")
        
        let clinicData: NSData = NSKeyedArchiver.archivedDataWithRootObject(clinic)
        userDefaults.setObject(clinicData, forKey: "clinicObject")
        userDefaults.synchronize()
        
        let readClinicData: NSData = userDefaults.objectForKey("clinicObject") as! NSData
        let readClinic: Clinic? = NSKeyedUnarchiver.unarchiveObjectWithData(readClinicData) as! Clinic
        
        XCTAssertNotNil(readClinic)
        
    }
    
}
