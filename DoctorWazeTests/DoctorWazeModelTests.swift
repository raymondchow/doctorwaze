//
//  DoctorWazeModelTests.swift
//  DoctorWaze
//
//  Created by Raymond Chow Chee Seng on 26/07/2016.
//  Copyright © 2016 Raymond Chow Chee Seng. All rights reserved.
//

import XCTest
@testable import DoctorWaze

class DoctorWazeModelTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testDoctorInitialization() {
        let newDoctor = Doctor(name: "Dr. Phil", specialization: Doctor.Specialization.Specialist)
        
    }
    
    func testClinicInitialization() {
        let firstDoctor = Doctor(name: "Dr ABC", specialization: Doctor.Specialization.Specialist)
        let secondDoctor = Doctor(name: "Dr DEF", specialization: Doctor.Specialization.General)
        
        var listOfDoctors: [Doctor] = []
        listOfDoctors += [firstDoctor]
        listOfDoctors += [secondDoctor]
        
        let newClinic = Clinic(name: "ClinicA", address: "Parklane OUG", contactNumber: "016-7778889", dateCreated: NSDate())
        
        print("Clinic Name \(newClinic.name)")
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measureBlock {
            // Put the code you want to measure the time of here.
        }
    }
    
}
