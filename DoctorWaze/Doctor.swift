//
//  Doctor.swift
//  DoctorWaze
//
//  Created by Raymond Chow Chee Seng on 23/07/2016.
//  Copyright © 2016 Raymond Chow Chee Seng. All rights reserved.
//

import Foundation


class Doctor{
    
    enum Specialization: String {
        case General
        case Specialist
    }
    
    let name : String
    var specialization: Specialization
    

    init(name: String, specialization: Specialization){
        self.name = name
        self.specialization = specialization
    }
    
    
}
