//
//  ClinicsListingViewController.swift
//  DoctorWaze
//
//  Created by Raymond Chow Chee Seng on 16/07/2016.
//  Copyright © 2016 Raymond Chow Chee Seng. All rights reserved.
//

import UIKit
var ListOfClinic : [Clinic] = []

class ClinicsListingViewController: UIViewController{
    
    @IBOutlet weak var tableView: UITableView!
    //var clinicModelController : ModelController = ModelController(
    
    var clinicListing: [Clinic] = []
    //let dataController: DataController = DataController()
    
    var refreshControl: UIRefreshControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.refreshClinicsListing()
        
        /*
        ClinicLoader.clinicLoader.getClinicsFromServer { (clinics: [Clinic]?) in
            // code
            if let clinics = clinics {
                print("Loaded: \(clinics)")
                self.clinicListing = clinics
                self.tableView.reloadData()
            } else {
                print("Couldn't load anything")
            }
            
        }
        */
        
        //self.clinicListing = self.dataController.returnObjectFromUserDefaults("CLNClinicsArray") as! Array
        //ListOfClinic = ModelController.generateClinics(3)

        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func viewDidAppear(animated: Bool)
    {
        self.refreshClinicsListing()
    }

    override func viewWillAppear(animated: Bool) {

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?){
        
        if segue.identifier == "presentAddClinicVC"
        {
            let destVC : AddTempClinicVC = segue.destinationViewController as! AddTempClinicVC
            destVC.clinicAdded = {(clinic: Clinic!) -> Void in
                //ListOfClinic += [clinic]
                self.clinicListing += [clinic]

                self.dismissViewControllerAnimated(true, completion: nil)
                self.tableView.reloadData()
            }
        }
        
        if segue.identifier == "presentAddViaDelegation"
        {
            let destVC : AddTempClinicDelegationVC = segue.destinationViewController as! AddTempClinicDelegationVC
            destVC.delegate = self
        }
        
        if segue.identifier == "showClinicDetailViewController"
        {
            let destVC: ClinicDetailViewController = segue.destinationViewController as! ClinicDetailViewController
            if let selectedIndexPath = self.tableView.indexPathForSelectedRow{
                let clinic: Clinic = self.clinicListing[selectedIndexPath.row]
                destVC.clinic = clinic
            }
            
    
        }
        
    }
    
    func refreshClinicsListing() {
        /*
        if let refreshControl = self.refreshControl {
            refreshControl.beginRefreshing()
        }
        */
        ClinicLoader.clinicLoader.getClinicsFromServer { (clinics: [Clinic]?) in
            // code
            if let clinics = clinics {
                self.clinicListing = clinics
                self.tableView.reloadData()
            }
            
        }
    }
    
}

extension ClinicsListingViewController : AddClinicDelegate {
    func viewController(vc: UIViewController, didAddClinic clinic: Clinic!) {
        ListOfClinic += [clinic]
        self.dismissViewControllerAnimated(true, completion: nil)
        self.tableView.reloadData()
    }
}


extension ClinicsListingViewController : UITableViewDataSource{
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //return ListOfClinic.count
        //return self.clinicModelController!.listOfClinic!.count
        return clinicListing.count

    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        //let cell: UITableViewCell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier:"Cell")
        let clinicsListingCell: ClinicsListingCell = self.tableView.dequeueReusableCellWithIdentifier("ClinicsListingCell", forIndexPath: indexPath) as!ClinicsListingCell
        //clinicsListingCell.clinic = ListOfClinic[indexPath.row]
        clinicsListingCell.clinic = clinicListing[indexPath.row]

        return clinicsListingCell
    }
}

extension ClinicsListingViewController: UITableViewDelegate{
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        print ("Selected row \(String(indexPath.row))")
    }
}