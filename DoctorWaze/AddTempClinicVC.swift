//
//  AddTempClinicVC.swift
//  DoctorWaze
//
//  Created by Raymond Chow Chee Seng on 26/07/2016.
//  Copyright © 2016 Raymond Chow Chee Seng. All rights reserved.
//

import UIKit

class AddTempClinicVC: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {


    @IBOutlet weak var detailsHolder: UIView!
    @IBOutlet weak var tempClinicNameText: UITextField!
    @IBOutlet weak var addTempClinicButton: UIButton!
    @IBOutlet weak var tempClinicContact: UITextField!
    @IBOutlet weak var tempClinicAddressText: UITextField!
    @IBOutlet weak var tempClinicEmailText: UITextField!
    @IBOutlet weak var tempClinicImage: UIImageView!

    var clinic : Clinic?
    var clinicAdded: ((clinic: Clinic!)->Void)?

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //imagePicker.delegate = self

        // Do any additional setup after loading the view.

        self.detailsHolder.layer.cornerRadius = 10
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func cancelAddClinic(sender: AnyObject) {
        self.presentingViewController?.dismissViewControllerAnimated(true, completion: nil)
        
    }
    
    @IBAction func addTempClinic(sender: AnyObject) {
        
        var name: String = ""
        var address: String = ""
        var contactNumber: String = ""
        var email: String = ""
        
        if self.tempClinicNameText.text != nil{
            name = self.tempClinicNameText.text!
        }
        if self.tempClinicAddressText.text != nil{
            address = self.tempClinicAddressText.text!
        }
        if self.tempClinicContact.text != nil{
            contactNumber  = self.tempClinicContact.text!
        }
        if self.tempClinicEmailText.text != nil{
            email = self.tempClinicEmailText.text!
        }
        
        self.clinic = Clinic(name: name, address: address, contactNumber: contactNumber)
        self.clinic?.email = email
        //self.clinicAdded?(clinic: self.clinic)
        
        addClinicToServer(self.clinic!)
    }
    
    func addClinicToServer(clinic: Clinic){
        ClinicLoader.clinicLoader.saveClinicToServer(clinic)
    }
    
    @IBAction func showSelectImageViewController(sender: AnyObject) {
        //let alertController: UIAlertController = UIAlertController(title: "Select Image", message: "You can now select an image :)", preferredStyle: .Alert)
        //alertController.addAction(UIAlertAction(title: "Great!", style: .Default, handler: nil))
        //self.presentViewController(alertController, animated: true, completion: nil)
        
        //let imagePickerView: UIImagePickerController = UIImagePickerController()
        
        let picker = UIImagePickerController()
        picker.allowsEditing = true
        picker.delegate = self
        presentViewController(picker, animated: true, completion: nil)
        
        
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]){
        // code
        let imageFromPicker: UIImage = info["UIImagePickerControllerEditedImage"] as! UIImage
        self.tempClinicImage.image = imageFromPicker
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController){
        //code
        dismissViewControllerAnimated(true, completion: nil)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
