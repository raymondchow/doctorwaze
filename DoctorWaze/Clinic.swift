//
//  Clinic.swift
//  DoctorWaze
//
//  Created by Raymond Chow Chee Seng on 19/07/2016.
//  Copyright © 2016 Raymond Chow Chee Seng. All rights reserved.
//

import Foundation
import UIKit
import MapKit

class Clinic : NSObject, NSCoding{
    
    var name : String
    var address : String
    var email : String?
    var contactNumber : String?
    var image: UIImage?
    var mapLocation : CLLocation?
    var dateCreated : NSDate
    //var doctorsAvailable : [Doctor]
    
    func encodeWithCoder(aCoder: NSCoder) {
        let keyedArchiver : NSKeyedArchiver = aCoder as! NSKeyedArchiver
        
        keyedArchiver.encodeObject(self.name, forKey: "CLNClinicName")
        keyedArchiver.encodeObject(self.address, forKey: "CLNClinicAddress")
        
    }
    
    required convenience init?(coder aDecoder: NSCoder) {
        let keyedUnArchiver : NSKeyedUnarchiver = aDecoder as! NSKeyedUnarchiver
        
        let name: String = keyedUnArchiver.decodeObjectForKey("CLNClinicName") as! String
        let address: String = keyedUnArchiver.decodeObjectForKey("CLNClinicAddress") as! String
        
        self.init(name: name, address: address, contactNumber: "")
        
    }
    
    init(name: String, address: String, contactNumber: String, dateCreated: NSDate=NSDate()) {
        self.name = name
        self.address = address
        self.contactNumber = contactNumber
        //self.doctorsAvailable = doctorsAvailable
        self.dateCreated=dateCreated
        
        super.init()
    }
    
}
