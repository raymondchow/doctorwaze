//
//  AddNewClinicViewController.swift
//  DoctorWaze
//
//  Created by Raymond Chow Chee Seng on 16/07/2016.
//  Copyright © 2016 Raymond Chow Chee Seng. All rights reserved.
//
import UIKit

class AddNewClinicViewController: UIViewController{
    
    @IBOutlet weak var mobileTextfield: UITextField!
    @IBOutlet weak var emailTextfield: UITextField!
    @IBOutlet weak var addressTextField: UITextField!
    @IBOutlet weak var nameTextfield: UITextField!
    @IBOutlet weak var imageLabel: UILabel!

    var doctors: [Doctor] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    @IBAction func saveRecord(sender: AnyObject) {
        
        let clinic: Clinic = Clinic(name: nameTextfield.text!, address: addressTextField.text!, contactNumber: mobileTextfield.text!)
       
        clinic.email=emailTextfield.text
        
        ListOfClinic.append(clinic)
    }
    
    @IBAction func uploadImage(sender: AnyObject) {
        imageLabel.text = String(ListOfClinic.count)
    }
}


extension AddNewClinicViewController : UITextFieldDelegate{
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        
        if textField == nameTextfield {
            addressTextField.becomeFirstResponder()
        } else if textField == addressTextField{
                emailTextfield.becomeFirstResponder()
        } else if textField == emailTextfield {
            mobileTextfield.becomeFirstResponder()
        } else if textField == mobileTextfield {
            textField.resignFirstResponder()
        }
        
        return true
    }
}