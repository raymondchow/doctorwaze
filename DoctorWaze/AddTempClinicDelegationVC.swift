//
//  AddTempClinicDelegationVC.swift
//  DoctorWaze
//
//  Created by Raymond Chow Chee Seng on 28/07/2016.
//  Copyright © 2016 Raymond Chow Chee Seng. All rights reserved.
//

import UIKit

protocol AddClinicDelegate{
    func viewController(vc: UIViewController, didAddClinic clinic: Clinic!)
}

class AddTempClinicDelegationVC: UIViewController {

    @IBOutlet weak var nameText: UITextField!
    @IBOutlet weak var addButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    
    var clinic: Clinic?
    var dummyDoctor: [Doctor] = []
    var delegate: AddClinicDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func cancel(sender: AnyObject) {
        self.presentingViewController?.dismissViewControllerAnimated(true, completion: nil)
    }

    @IBAction func addClinic(sender: AnyObject) {
        self.clinic = Clinic(name: self.nameText.text!, address: "", contactNumber:"", dateCreated: NSDate())
        self.delegate?.viewController(self, didAddClinic: self.clinic)
        
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
