//
//  ModelController.swift
//  DoctorWaze
//
//  Created by Raymond Chow Chee Seng on 23/07/2016.
//  Copyright © 2016 Raymond Chow Chee Seng. All rights reserved.
//

import Foundation

class ModelController {
    
    var listOfClinic: [Clinic]?
    var listOfDoctor: [Doctor]?
    
    
    class func generateDoctors (count: Int) -> [Doctor]{
        
        var listOfDoctors: [Doctor] = []
        
        
        for i in 0 ..< count
        {
            let newDoctor = Doctor(name: "Doctor" + String(i), specialization: Doctor.Specialization.General)
            listOfDoctors += [newDoctor]
        }
        
        
        return listOfDoctors
    }
    
    class func generateClinics (count: Int) -> [Clinic] {
        var listOfClinics: [Clinic] = []
        
        let doctorsAvailable: [Doctor] = generateDoctors(2)
        
        for i in 0 ..< count
        {
            let newClinic = Clinic(name: "Clinic BARU" + String(i), address: "Malaysia", contactNumber: "999")
            listOfClinics += [newClinic]
        }
        
        return listOfClinics
    }
    
}
