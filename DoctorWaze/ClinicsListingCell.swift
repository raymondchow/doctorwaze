//
//  ClinicsListingCell.swift
//  DoctorWaze
//
//  Created by Raymond Chow Chee Seng on 19/07/2016.
//  Copyright © 2016 Raymond Chow Chee Seng. All rights reserved.
//

import UIKit

class ClinicsListingCell: UITableViewCell {
    
    @IBOutlet private weak var imageClinicImageView: UIImageView!
    //@IBOutlet private weak var mobileClinicLabelView: UILabel!
    //@IBOutlet private weak var emailClinicLabelView: UILabel!
    @IBOutlet private weak var addressClinicLabelView: UILabel!
    @IBOutlet private weak var nameClinicLabelView: UILabel!
    
    var clinic: Clinic! {
        didSet {
            self.nameClinicLabelView?.text = self.clinic.name
            self.addressClinicLabelView?.text = self.clinic.address
            //self.mobileClinicLabelView?.text = self.clinic.contactNumber
            //self.emailClinicLabelView?.text = self.clinic.email

        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()

        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
