//
//  ClinicDetailViewController.swift
//  DoctorWaze
//
//  Created by Raymond Chow Chee Seng on 23/07/2016.
//  Copyright © 2016 Raymond Chow Chee Seng. All rights reserved.
//

import UIKit

protocol DetailViewDelegate{
    func viewController(vc: UIViewController, viewClinic clinic: Clinic!)
}

class ClinicDetailViewController: UIViewController {
    
    var delegate: DetailViewDelegate?

    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var contactNumberLabel: UILabel!
    @IBOutlet weak var addressNameLabel: UILabel!

    @IBOutlet weak var clinicNameLabel: UILabel!
    var clinic: Clinic!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.clinicNameLabel.text = self.clinic.name
        self.addressNameLabel.text = self.clinic.address
        self.contactNumberLabel.text = self.clinic.contactNumber
        self.emailLabel.text = self.clinic.email

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func cancelViewClinic(sender: AnyObject) {
        self.presentingViewController?.dismissViewControllerAnimated(true, completion: nil)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?){
        
    
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
