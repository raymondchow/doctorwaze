//
//  ClinicLoader.swift
//  DoctorWaze
//
//  Created by Raymond Chow Chee Seng on 30/07/2016.
//  Copyright © 2016 Raymond Chow Chee Seng. All rights reserved.
//

import Foundation
import UIKit

class ClinicLoader {
    
    // Singleton pattern
    static let clinicLoader: ClinicLoader = ClinicLoader()
    
    
    // File Manager Object
    let fileManager: NSFileManager = NSFileManager.defaultManager()
    
    
    //NSURL
    let url: NSURL
    //NSURLConfiguration
    let getSessionConfig: NSURLSessionConfiguration
    let postSessionConfig: NSURLSessionConfiguration
    //NSURLSession
    let getSession: NSURLSession
    let postSession: NSURLSession
    
    private init() {
    
        self.url = NSURL(string: "https://api.backendless.com/v1/data/Clinic")!;
        
        self.getSessionConfig = NSURLSessionConfiguration.defaultSessionConfiguration();
        self.getSessionConfig.HTTPAdditionalHeaders = ["application-id" : "AEECCF30-FAE7-BF12-FFC6-2E0AD139DB00",
                                                       "secret-key" : "BEC01580-9EC4-D4B7-FF3A-3D2EF2198100"]
        
        self.postSessionConfig = NSURLSessionConfiguration.defaultSessionConfiguration();
        self.postSessionConfig.HTTPAdditionalHeaders = ["application-id" : "AEECCF30-FAE7-BF12-FFC6-2E0AD139DB00",
                                                       "secret-key" : "BEC01580-9EC4-D4B7-FF3A-3D2EF2198100",
                                                       "Content-type" : "application/json",
                                                       "application-type" : "REST"]
        
        self.getSession = NSURLSession(configuration: self.getSessionConfig)
        self.postSession = NSURLSession(configuration: self.postSessionConfig)
        
    }
    
    
    func getFileURL() -> NSURL {
        let documentsDirectory: NSURL = fileManager.URLsForDirectory(NSSearchPathDirectory.DocumentDirectory, inDomains: NSSearchPathDomainMask.UserDomainMask).first!
        
        let filePath: NSURL = documentsDirectory.URLByAppendingPathComponent("data.plist")
        
        return filePath
        
    }
    
    
    func getClinicsFromFile() -> [Clinic] {
        var clinicListing: [Clinic] = []
        
        if let parkData: [NSData] = NSArray(contentsOfURL: self.getFileURL()) as? [NSData] {
            for recordData in parkData {
                let clinic: Clinic = NSKeyedUnarchiver.unarchiveObjectWithData(recordData) as! Clinic
                clinicListing += [clinic]
            }
        }
        
        return clinicListing
    }

    
    func saveClinicToFile(clinics: [Clinic]) {

        var clinicListing: [Clinic] = []
        
        clinicListing = clinics
        
        var clinicsData: [NSData] = []
        
        for clinic in clinicListing{
            let data: NSData = NSKeyedArchiver.archivedDataWithRootObject(clinic)
            clinicsData += [data]
        }
        
        (clinicsData as NSArray).writeToURL(self.getFileURL(), atomically: true)

    }
    
    func saveClinicToServer(clinic: Clinic) {
        
        // Define the URL Request Object
        // NSMutableRequest
        let urlRequest: NSMutableURLRequest = NSMutableURLRequest(URL: self.url)
        
        // Set the METHOD for URL Request
        urlRequest.HTTPMethod = "POST"
        
        
        // Define a JSON object for the Clinic object created
        let jsonClinic: [String : AnyObject] = ["ClinicName" : clinic.name,
                                                "ClinicEmail" : clinic.email ??  NSNull(),
                                                "ClinicContact" : clinic.contactNumber ?? NSNull(),
                                                "ClinicAddress" : clinic.address ?? NSNull()
                                                ]
        
        
        // Create a JSON DATA
        // Serialize the JSON Object to JSON Data
        let jsonClinicData: NSData = try! NSJSONSerialization.dataWithJSONObject(jsonClinic, options: NSJSONWritingOptions(rawValue: 0))
        
        // Set the BODY of the URL Request
        // Usually expecting JSON Data
        urlRequest.HTTPBody = jsonClinicData
        
        // Define the session task
        // This time is using Session Task with URL Request
        let postTask: NSURLSessionDataTask = self.postSession.dataTaskWithRequest(urlRequest) { (data: NSData?, response: NSURLResponse?, error: NSError?) in
            //code
            print(response)
            
        }
        postTask.resume()
        
    }

    func getClinicsFromServer(completionBlock: ((clinics: [Clinic]?) -> Void)? ) {
        // URL
        //let url: NSURL =  NSURL(string: "https://api.backendless.com/v1/data/Clinic")!
        
        // need to load an URL
        //let sessionConfig: NSURLSessionConfiguration = NSURLSessionConfiguration.defaultSessionConfiguration()
        
        //sessionConfig.HTTPAdditionalHeaders = ["application-id" : "AEECCF30-FAE7-BF12-FFC6-2E0AD139DB00",
                                               //"secret-key" : "BEC01580-9EC4-D4B7-FF3A-3D2EF2198100"]
        
        // Assign  configuration to Session
        //let session: NSURLSession = NSURLSession(configuration: sessionConfig)
        
        
        let loadTask: NSURLSessionDataTask = self.getSession.dataTaskWithURL(self.url) { (data: NSData?, response: NSURLResponse?, error: NSError?) in
            
            //let abc: String? = String(data: data!, encoding: NSUTF8StringEncoding)
            // Convert data to JSON Object
            
            // Define a dictionary key-object
            // Then try to deserialize the data object return using NJSONDeserilization
            let jsonObject : [String : AnyObject] = try! NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions(rawValue : 0)) as! [String : AnyObject]
            
            let dataArray: [[String : AnyObject]] = (jsonObject["data"] as? [[String : AnyObject]])!
            
            var clinicList: [Clinic] = []
            for clinicJSON in dataArray{
                let name: String = clinicJSON["ClinicName"] as! String
                let address: String = clinicJSON["ClinicAddress"] as! String
                let contactNumber: String = clinicJSON["ClinicContact"] as! String
                let email: String = clinicJSON["ClinicEmail"] as! String
                //let image: UIImage = clinicJSON["ImageFile"] as! UIImage
                
                let clinic: Clinic = Clinic(name: name, address: address, contactNumber: contactNumber)
                clinic.email = email
                //clinic.image = image
                clinicList.append(clinic)
            }

            dispatch_async(dispatch_get_main_queue(), {
                completionBlock?(clinics: clinicList)
            })
        }
        
        loadTask.resume()
    }
 
}
